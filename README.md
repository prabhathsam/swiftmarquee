# SwiftMarquee

[![CI Status](https://img.shields.io/travis/prabhathsam/SwiftMarquee.svg?style=flat)](https://travis-ci.org/prabhathsam/SwiftMarquee)
[![Version](https://img.shields.io/cocoapods/v/SwiftMarquee.svg?style=flat)](https://cocoapods.org/pods/SwiftMarquee)
[![License](https://img.shields.io/cocoapods/l/SwiftMarquee.svg?style=flat)](https://cocoapods.org/pods/SwiftMarquee)
[![Platform](https://img.shields.io/cocoapods/p/SwiftMarquee.svg?style=flat)](https://cocoapods.org/pods/SwiftMarquee)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

var marquee = Marquee()
marquee.mainView = self.view
marquee.timeInterval = 5
marquee.paddingTop = 100
marquee.labelColor = UIColor.red
marquee.tickers = [
    "asdsad",
    "asdasdsss",
    "asdsads",
    "ssdadxcxczx"
]
marquee.start()

## Requirements

## Installation

SwiftMarquee is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SwiftMarquee'
```

## Author

prabhathsam, prabhath.samarathunga@gmail.com

## License

SwiftMarquee is available under the MIT license. See the LICENSE file for more info.
# SwiftMarquee
