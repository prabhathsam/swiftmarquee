//
//  ViewController.swift
//  SwiftMarquee
//
//  Created by prabhathsam on 12/02/2020.
//  Copyright (c) 2020 prabhathsam. All rights reserved.
//

import UIKit
import SwiftMarquee

class ViewController: UIViewController {

    @IBOutlet weak var marqueeView: UIView!
    
    var marquee = Marquee()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        marquee.mainView = self.view
        marquee.timeInterval = 5
        marquee.paddingTop = 100
        marquee.labelColor = UIColor.red
        marquee.tickers = [
            "asdsad",
            "asdasdsss",
            "asdsads",
            "ssdadxcxczx"
        ]
        marquee.start()
    }

    @IBAction func btnClicked(_ sender: Any) {
        marquee.stop()
    }
    
    @IBAction func btn1Clicked(_ sender: Any) {
        marquee .start()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}

